package com.starkogi.mamamboga.Utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.starkogi.mamamboga.Models.Item;
import com.starkogi.mamamboga.R;

public class ItemsViewHolder extends RecyclerView.ViewHolder {

    public View view;
    ImageView img_image;
    TextView tv_item_name,tv_item_description, tv_item_price;
    Button btn_select;

    Context ctx;

    public ItemsViewHolder(View v, Context ctx) {
        super(v);
        view = v;
        this.ctx = ctx;

        img_image = v.findViewById(R.id.img_image);
        tv_item_name = v.findViewById(R.id.tv_item_name);
        tv_item_description = v.findViewById(R.id.tv_item_description);
        tv_item_price = v.findViewById(R.id.tv_item_price);
        btn_select = v.findViewById(R.id.btn_select);

    }

    public void bind(Item item) {
        Log.e("Item", new Gson().toJson(item));

        tv_item_price.setText(Integer.toString(item.getItemPrice()));
        tv_item_description.setText(item.getItemDescription());
        tv_item_name.setText(item.getItemName());

        Picasso.with(ctx)
                .load(item.getImage())
                .placeholder(R.drawable.placeholder)
                .into(img_image);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onItemClick(v, getAdapterPosition());

            }
        });
        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mClickListener.onItemLongClick(v, getAdapterPosition());
                return true;
            }
        });
    }
    private ItemsViewHolder.ClickListener mClickListener;


    //Interface to send callbacks...
    public interface ClickListener{
        public void onItemClick(View view, int position);
        public void onItemLongClick(View view, int position);
    }

    public void setOnClickListener(ItemsViewHolder.ClickListener clickListener){
        mClickListener = clickListener;
    }
}

