package com.starkogi.mamamboga;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.starkogi.mamamboga.Models.ShippingDetails;

public class ShippingDetailsActivity extends AppCompatActivity {


    private Button btn_save_proceed;

    private EditText et_pobox;
    private EditText et_phone;
    private EditText et_landmark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_details);

        initComponents();
    }

    private void initComponents() {
        btn_save_proceed = findViewById(R.id.btn_save_proceed);
        et_pobox = findViewById(R.id.et_pobox);
        et_phone = findViewById(R.id.et_phone);
        et_landmark = findViewById(R.id.et_landmark);

        onClickListeners();
    }

    private void onClickListeners() {
        btn_save_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
    }

    private void saveData() {
        String Pobox = et_pobox.getText().toString().trim();
        String Phone = et_phone.getText().toString().trim();
        String LMark = et_landmark.getText().toString().trim();

        if(TextUtils.isEmpty(Pobox)){
            et_pobox.setError("Provide Details");

        }else if(TextUtils.isEmpty(Phone)){
            et_phone.setError("Provide Details");

        }else if(TextUtils.isEmpty(LMark)){
            et_landmark.setError("Provide Details");
        }else {

            ShippingDetails details = new ShippingDetails(
                    FirebaseAuth.getInstance().getUid(), LMark, Pobox, Phone
            );

            FirebaseDatabase.getInstance().getReference().child("shipping_details").child(details.getUid()).setValue(details)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    startActivity(new Intent(getApplicationContext(), Cart.class));
                    finish();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Failed. Please Try Again : " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}

