package com.starkogi.mamamboga.Models;

public class CategoriesSlider {
    private String categotyName, image;

    public CategoriesSlider() {
    }

    public CategoriesSlider(String categotyName, String image) {
        this.categotyName = categotyName;
        this.image = image;
    }

    public String getCategotyName() {
        return categotyName;
    }

    public void setCategotyName(String categotyName) {
        this.categotyName = categotyName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
