package com.starkogi.mamamboga.Models;

import android.os.Parcel;
import android.os.Parcelable;


public class Cart_Item implements Parcelable {
    private String itemName;
    private String itemDescription;
    private String itemUid;
    private int itemPrice;
    private int itemCount;
    private String image;
    private int quantity;
    private String uid;
    private String timeAdded;

    public Cart_Item() {}  // Needed for Firebase

    public Cart_Item(String itemName, String itemDescription, String itemUid, int itemPrice, int itemCount,
                     String image, int quantity, String uid, String timeAdded) {
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemUid = itemUid;
        this.itemPrice = itemPrice;
        this.itemCount = itemCount;
        this.image = image;
        this.quantity = quantity;
        this.uid = uid;
        this.timeAdded = timeAdded;
    }

    public Cart_Item(String itemName, String itemDescription, String itemUid, int itemPrice, int itemCount,
                     String image, int quantity, String uid) {
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemUid = itemUid;
        this.itemPrice = itemPrice;
        this.itemCount = itemCount;
        this.image = image;
        this.quantity = quantity;
        this.uid = uid;
    }

    protected Cart_Item(Parcel in) {
        itemName = in.readString();
        itemDescription = in.readString();
        itemUid = in.readString();
        itemPrice = in.readInt();
        itemCount = in.readInt();
        image = in.readString();
        quantity  = in.readInt();
        uid  = in.readString();
        timeAdded  = in.readString();
    }

    public static final Creator<Cart_Item> CREATOR = new Creator<Cart_Item>() {
        @Override
        public Cart_Item createFromParcel(Parcel in) {
            return new Cart_Item(in);
        }

        @Override
        public Cart_Item[] newArray(int size) {
            return new Cart_Item[size];
        }
    };

    public int getQuantity() {
        return quantity;
    }

    public String getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(String timeAdded) {
        this.timeAdded = timeAdded;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemUid() {
        return itemUid;
    }

    public void setItemUid(String itemUid) {
        this.itemUid = itemUid;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemName);
        dest.writeString(itemDescription);
        dest.writeString(itemUid);
        dest.writeInt(itemPrice);
        dest.writeInt(itemCount);
        dest.writeString(image);
    }
}
