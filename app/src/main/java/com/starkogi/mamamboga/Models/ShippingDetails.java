package com.starkogi.mamamboga.Models;

public class ShippingDetails {
    private String uid;
    private String landmark;
    private String po_box_number;
    private String phone_number;

    public ShippingDetails() {
    }

    public ShippingDetails(String uid, String landmark, String po_box_number, String phone_number) {
        this.uid = uid;
        this.landmark = landmark;
        this.po_box_number = po_box_number;
        this.phone_number = phone_number;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getPo_box_number() {
        return po_box_number;
    }

    public void setPo_box_number(String po_box_number) {
        this.po_box_number = po_box_number;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
