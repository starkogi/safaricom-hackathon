package com.starkogi.mamamboga;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.starkogi.mamamboga.Fragments.Items;
import com.starkogi.mamamboga.Models.Cart_Item;
import com.starkogi.mamamboga.Models.Item;
import com.starkogi.mamamboga.Models.ShippingDetails;
import com.starkogi.mamamboga.MpesaApi.Utils;
import com.starkogi.mamamboga.MpesaApi.api.ApiClient;
import com.starkogi.mamamboga.MpesaApi.api.model.AccessToken;
import com.starkogi.mamamboga.MpesaApi.api.model.STKPush;
import com.starkogi.mamamboga.Utils.ItemsViewHolder;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

import static com.starkogi.mamamboga.MpesaApi.AppConstants.BUSINESS_SHORT_CODE;
import static com.starkogi.mamamboga.MpesaApi.AppConstants.CALLBACKURL;
import static com.starkogi.mamamboga.MpesaApi.AppConstants.PARTYB;
import static com.starkogi.mamamboga.MpesaApi.AppConstants.PASSKEY;
import static com.starkogi.mamamboga.MpesaApi.AppConstants.TRANSACTION_TYPE;

public class Cart extends AppCompatActivity {

    private RecyclerView items_list;
    FloatingActionButton fab_pay;
    TextView tv_total;

    private ArrayList<Cart_Item> cart_items = new ArrayList<>();

    private Dialog myDialog;
    private ProgressDialog mProgressDialog;
    private ApiClient mApiClient;

    private RecyclerView.LayoutManager mLayoutManager;

    int total_amount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        initComponents();
    }

    private void initComponents() {
        items_list = findViewById(R.id.items_list);
        tv_total = findViewById(R.id.tv_total);
        fab_pay = findViewById(R.id.fab_pay);

        mApiClient = new ApiClient();
        mApiClient.setIsDebug(true); //Set True to enable logging, false to disable.

        getAccessToken();

        myDialog = new Dialog(this);
        mProgressDialog = new ProgressDialog(this);

        tv_total.setText(Integer.toString(0));

        fab_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseDatabase.getInstance().getReference().child("shipping_details").child(FirebaseAuth.getInstance().getUid()).addValueEventListener(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if(dataSnapshot.exists()){

                                    ShippingDetails details = dataSnapshot.getValue(ShippingDetails.class);

                                    if(details != null){
                                        ShowPopup();
                                    }else {
                                        startActivity(new Intent(getApplicationContext(), ShippingDetailsActivity.class));
                                    }
                                }else {
                                    startActivity(new Intent(getApplicationContext(), ShippingDetailsActivity.class));

                                }


                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getApplicationContext(), "Failed. Please Try Again : " + databaseError.getMessage(), Toast.LENGTH_LONG).show();

                            }
                        }
                );

            }
        });

        tv_total.setText(Integer.toString(total_amount));

        setTotal_amount();

        setupRV();
    }


    private void setupRV() {


// use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView

        // use a linear layout manager
        mLayoutManager =  new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);

        items_list.setLayoutManager(mLayoutManager); // set LayoutManager to RecyclerView

        attachRecyclerViewAdapter();

    }

    private void attachRecyclerViewAdapter() {
        final RecyclerView.Adapter adapter = newAdapter();

        // Scroll to bottom on new messages
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                //items_list.smoothScrollToPosition(positionStart);
            }
        });

        items_list.setAdapter(adapter);
    }
    public void getAccessToken() {

        mApiClient.setGetAccessToken(true);
        mApiClient.mpesaService().getAccessToken().enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, retrofit2.Response<AccessToken> response) {
                if (response.isSuccessful()) {
                    mApiClient.setAuthToken(response.body().accessToken);
                    //Toast.makeText(getApplicationContext(), response.body().accessToken, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {

            }
        });
    }

    public void ShowPopup() {
        final TextView txtclose, tv_amount_due_pop, tv_prev_reading_pop, tv_current_reading_pop, tv_acc_no_pop;
        final EditText et_amount, et_phone_number;
        Button btn_pay;
        myDialog.setContentView(R.layout.custom_pay_popup);
        myDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);

        et_amount = myDialog.findViewById(R.id.et_amount);
        tv_amount_due_pop = myDialog.findViewById(R.id.tv_amount_due_pop);
        et_phone_number = myDialog.findViewById(R.id.et_phone_number);

        et_amount.setText(Integer.toString(total_amount));
        tv_amount_due_pop.setText(Integer.toString(total_amount));

        txtclose = myDialog.findViewById(R.id.txtclose);
        btn_pay = (Button) myDialog.findViewById(R.id.btn_pay);
        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(et_amount.getText().toString()) && Integer.parseInt(et_amount.getText().toString()) > 0 ){
                    performSTKPush(Integer.parseInt(et_amount.getText().toString()), et_phone_number.getText().toString());
                }else {
                    et_amount.setError("Add Amount to be paid");
                }
            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    public void performSTKPush(int amount, String phone) {
        mProgressDialog.setMessage("Please Wait");
        mProgressDialog.setTitle("Mpesa Payment");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
        String timestamp = Utils.getTimestamp();
        STKPush stkPush = new STKPush(
                BUSINESS_SHORT_CODE,
                Utils.getPassword(BUSINESS_SHORT_CODE, PASSKEY, timestamp),
                timestamp,
                TRANSACTION_TYPE,
                String.valueOf(amount),
                Utils.sanitizePhoneNumber(phone),
                PARTYB,
                Utils.sanitizePhoneNumber(phone),
                CALLBACKURL + Utils.getTimestamp(),
                Utils.sanitizePhoneNumber(phone), //The account reference
                "Items Bought. Date : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date())  //The transaction description
        );

        mApiClient.setGetAccessToken(false);

        mApiClient.mpesaService().sendPush(stkPush).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                mProgressDialog.dismiss();
                View view = getLayoutInflater().inflate(R.layout.activity_main, null);

                mProgressDialog.dismiss();
                finish();
                if (response.body().has("ResponseCode")){

                    Toast.makeText(getApplicationContext(), response.body().get("ResponseCode").toString(), Toast.LENGTH_LONG).show();

                    if(response.body().get("ResponseCode").toString().equalsIgnoreCase("0")){


                        for (Cart_Item item : cart_items) {
                            item.setTimeAdded(Utils.getTimestamp());

                            FirebaseDatabase.getInstance().getReference().child("orders").child(FirebaseAuth.getInstance().getUid())
                                    .child(Utils.getTimestamp()).setValue(item);
                        }
                    }

                }else {
                    Toast.makeText(getApplicationContext(), "Error Response " + response.message(), Toast.LENGTH_LONG).show();


                }
//                AccessToken accessToken = new Gson().fromJson(response.toString(),
//                        AccessToken.class);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mProgressDialog.dismiss();
//                AccessToken accessToken = new Gson().fromJson(response.toString(),
//                        AccessToken.class);
                Toast.makeText(getApplicationContext(), "Error Response " + t.getMessage(), Toast.LENGTH_LONG).show();
            }


        });
    }

    public void setTotal_amount() {
        FirebaseDatabase.getInstance().getReference().child("cart").child(FirebaseAuth.getInstance().getUid()).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                        for ( DataSnapshot item : dataSnapshot.getChildren()) {
                            total_amount += item.getValue(Cart_Item.class).getQuantity() * item.getValue(Cart_Item.class).getItemPrice();

                            cart_items.add(item.getValue(Cart_Item.class));
                        }

                        tv_total.setText(Integer.toString(total_amount));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                }
        );
    }

    //Get Cart Data
    protected RecyclerView.Adapter newAdapter() {

        Query sItemQuery =
                FirebaseDatabase.getInstance().getReference().child("cart").child(FirebaseAuth.getInstance().getUid());

        FirebaseRecyclerOptions<Cart_Item> options =
                new FirebaseRecyclerOptions.Builder<Cart_Item>()
                        .setQuery(sItemQuery, Cart_Item.class)
                        .setLifecycleOwner(this)
                        .build();

        return new FirebaseRecyclerAdapter<Cart_Item, ItemsViewHolder>(options) {

            @NonNull
            @Override
            public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                ItemsViewHolder holder = new ItemsViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_cart_item_view, parent, false), getApplicationContext());

                return holder;
            }

            @Override
            protected void onBindViewHolder(@NonNull ItemsViewHolder holder, int position, @NonNull Cart_Item model) {
                holder.bind(model);

            }
        };
    }

    public class ItemsViewHolder extends RecyclerView.ViewHolder {

        public View view;
        ImageView img_image;
        TextView tv_item_name,tv_item_description, tv_item_price, tv_item_quantity;
        Button btn_remove_item, btn_reduce_quantity, btn_add_quantity;

        Context ctx;

        public ItemsViewHolder(View v, Context ctx) {
            super(v);
            view = v;
            this.ctx = ctx;

            img_image = v.findViewById(R.id.img_image);
            tv_item_name = v.findViewById(R.id.tv_item_name);
            tv_item_description = v.findViewById(R.id.tv_item_description);
            tv_item_price = v.findViewById(R.id.tv_item_price);
            btn_remove_item = v.findViewById(R.id.btn_remove_item);
            tv_item_quantity = v.findViewById(R.id.tv_item_quantity);
            btn_add_quantity = v.findViewById(R.id.btn_add_quantity);
            btn_reduce_quantity = v.findViewById(R.id.btn_reduce_quantity);

        }

        public void bind(Cart_Item item) {
            Log.e("Item", new Gson().toJson(item));

            tv_item_price.setText(Integer.toString(item.getItemPrice()));
            tv_item_description.setText(item.getItemDescription());
            tv_item_name.setText(item.getItemName());
            tv_item_quantity.setText(Integer.toString(item.getQuantity()));

            Picasso.with(ctx)
                    .load(item.getImage())
                    .placeholder(R.drawable.placeholder)
                    .into(img_image);

            btn_remove_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            btn_add_quantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            btn_reduce_quantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

}
